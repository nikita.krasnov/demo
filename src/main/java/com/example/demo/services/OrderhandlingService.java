package com.example.demo.services;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class OrderhandlingService {

    private volatile AtomicInteger counter = new AtomicInteger();

    @KafkaListener(topics = {"orders"})
    public void handle(@Payload String message) {
        //do some handling
        counter.incrementAndGet();
    }

    public int getCount(){
        return counter.get();
    }

}
