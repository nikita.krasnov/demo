package com.example.demo.controllers;

import com.example.demo.services.OrderhandlingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculationController {

    @Autowired
    private OrderhandlingService orderhandlingService;

    @GetMapping("/orders/count")
    @ResponseBody
    public Integer getHandledCount(){
        return orderhandlingService.getCount();
    }
}
